# Instalación

## Gradle
```groovy
repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/55325175/packages/maven'
    }
}

dependencies {
    implementation 'com.gitlab.josercl:spring-boot-export-utils:1.0.3'
}
```

## Maven

```xml
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/55325175/packages/maven</url>
    </repository>
</repositories>

<distributionManagement>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/55325175/packages/maven</url>
    </repository>
    
    <snapshotRepository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/55325175/packages/maven</url>
    </snapshotRepository>
</distributionManagement>

<dependencies>
    <dependency>
        <groupId>com.gitlab.josercl</groupId>
        <artifactId>spring-boot-export-utils</artifactId>
        <version>1.0.3</version>
    </dependency>
</dependencies>
```

# Uso

```java
public class SomeClass {
    private final Exporter exporter;
    
    public File someMethod() {
        return exporter.exportToFile(
                () -> getListOfSomething(),
                FileType.EXCEL,
                Column.<Something>builder().order(0).header("Header1").mapper(something -> something.getField1()).build(),
                Column.<Something>builder().order(1).header("Header2").mapper(something -> something.getField2()).build(),
                ...,
                Column.<Something>builder().order(N).header("HeaderN").mapper(something -> something.getFieldN()).build()
        );
    }
}
```