package com.gitlab.josercl.export;

import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.exception.ExportException;
import com.gitlab.josercl.export.writer.Column;
import com.gitlab.josercl.export.writer.Writer;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

public class DefaultExporter extends Exporter {
    private final List<Writer> writers;

    public DefaultExporter(List<Writer> writers) {
        this.writers = writers;
    }

    @Override
    public <T> File exportToFile(Supplier<Collection<T>> dataSupplier, FileType fileType, Set<Column<T>> columns) {
        if (Objects.isNull(fileType)) {
            throw new ExportException("FileType cannot be null");
        }

        Writer writer = writers.stream()
                .filter(value -> value.canWrite(fileType))
                .findFirst()
                .orElseThrow(() -> new ExportException("Could not find a writer for fileType: " + fileType.name()));

        try {
            return writer.toFile(dataSupplier.get(), columns);
        } catch (Exception exception) {
            throw new ExportException(exception.getMessage(), exception);
        }
    }
}
