package com.gitlab.josercl.export.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gitlab.josercl.export.DefaultExporter;
import com.gitlab.josercl.export.Exporter;
import com.gitlab.josercl.export.writer.CSVWriter;
import com.gitlab.josercl.export.writer.ExcelWriter;
import com.gitlab.josercl.export.writer.JSONWriter;
import com.gitlab.josercl.export.writer.TextWriter;
import com.gitlab.josercl.export.writer.Writer;
import com.gitlab.josercl.export.writer.XMLWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import java.util.List;

@AutoConfiguration
public class ExporterAutoConfiguration {
    @Bean
    public Exporter exporter(List<Writer> writers) {
        return new DefaultExporter(writers);
    }

    @Bean
    @ConditionalOnMissingBean
    public XMLWriter xmlWriter() {
        return new XMLWriter(new XmlMapper());
    }

    @Bean
    @ConditionalOnMissingBean
    public ExcelWriter excelWriter(
            @Value("${export.excel.date-format:Y-m-d}") String dateFormat,
            @Value("${export.excel.decimal-places:2}") int decimalPlaces,
            ExcelWriter.HeaderCellCustomizer headerCellCustomizer,
            ExcelWriter.ContentCellCustomizer contentCellCustomizer
    ) {
        return new ExcelWriter(dateFormat, decimalPlaces, headerCellCustomizer, contentCellCustomizer);
    }

    @Bean
    @ConditionalOnMissingBean
    public ExcelWriter.HeaderCellCustomizer excelHeaderCustomizer() {
        return (cellStyle, workbook) -> {
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public ExcelWriter.ContentCellCustomizer excelContentCustomizer() {
        return (cellStyle, workbook, row) -> {
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public TextWriter textWriter(
            @Value("${export.text.column-separation:5}") int columnSeparation
    ) {
        return new TextWriter(columnSeparation);
    }

    @Bean
    @ConditionalOnMissingBean
    public CSVWriter csvWriter(
            @Value("${export.csv.separator:;}") String separator
    ) {
        return new CSVWriter(separator);
    }

    @Bean
    @ConditionalOnMissingBean
    public JSONWriter jsonWriter(ObjectMapper objectMapper) {
        return new JSONWriter(objectMapper);
    }
}
