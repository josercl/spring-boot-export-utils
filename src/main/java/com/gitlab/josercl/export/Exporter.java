package com.gitlab.josercl.export;

import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.writer.Column;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public abstract class Exporter {
    abstract <T> File exportToFile(
            Supplier<Collection<T>> dataSupplier,
            FileType fileType,
            Set<Column<T>> columns
    );

    @SafeVarargs
    public final <T> File exportToFile(
            Supplier<Collection<T>> dataSupplier,
            FileType fileType,
            Column<T> ...columns
    ) {
        return exportToFile(dataSupplier, fileType, Arrays.stream(columns).collect(Collectors.toSet()));
    }
}
