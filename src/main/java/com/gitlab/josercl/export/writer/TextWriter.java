package com.gitlab.josercl.export.writer;

import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.exception.ExportException;
import com.gitlab.josercl.export.util.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextWriter extends Writer {
    private final int columnSeparation;

    public TextWriter(int columnSeparation) {
        this.columnSeparation = columnSeparation;
    }

    @Override
    public boolean canWrite(FileType fileType) {
        return FileType.TXT.equals(fileType);
    }

    @Override
    public <T> File toFile(Collection<T> originalData, Set<Column<T>> columns) {
        Map<String, Integer> columnWidths = new HashMap<>();

        List<Column<T>> columnList = columns.stream().sorted().toList();

        List<String> headers = columnList.stream()
                .map(Column::getHeader)
                .toList();

        columnList.forEach(col -> columnWidths.put(col.getHeader(), col.getHeader().length()));

        var records = originalData.stream()
                .map(record -> {
                    Map<String, Object> fields = new HashMap<>();

                    for (Column<T> tColumn : columnList) {
                        Function<T, Object> mapper = tColumn.getMapper();

                        String value = mapper.apply(record).toString();
                        String header = tColumn.getHeader();

                        columnWidths.put(header, Math.max(columnWidths.get(header), value.length()));

                        fields.put(header, value);
                    }

                    return fields;
                })
                .toList();

        File file = getFile();
        try(FileWriter writer = new FileWriter(file)) {
            headers.forEach(header -> write(writer, StringUtils.rightPad(header, columnWidths.get(header) + columnSeparation)));
            write(writer, System.lineSeparator());
            records.forEach(record -> {
                String line = headers.stream().map(header -> {
                    String value = record.get(header).toString();
                    return StringUtils.rightPad(value, columnWidths.get(header) + columnSeparation);
                }).collect(Collectors.joining());

                write(writer, line);
                write(writer, System.lineSeparator());
            });
        } catch (IOException e) {
            throw new ExportException(e.getMessage(), e);
        }

        return file;
    }
}
