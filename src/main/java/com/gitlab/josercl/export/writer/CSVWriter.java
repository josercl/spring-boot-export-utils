package com.gitlab.josercl.export.writer;

import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.exception.ExportException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class CSVWriter extends Writer {

    private static final String CSV_SEPARATOR = ";";

    private final String fieldSeparator;

    public CSVWriter() {
        this(CSV_SEPARATOR);
    }

    public CSVWriter(String separator) {
        this.fieldSeparator = separator;
    }

    @Override
    public boolean canWrite(FileType fileType) {
        return FileType.CSV.equals(fileType);
    }

    @Override
    public <T> File toFile(Collection<T> originalData, Set<Column<T>> columns) {
        List<String> headers = columns.stream()
                .sorted()
                .map(Column::getHeader)
                .toList();

        List<Function<T, Object>> mappers = columns.stream()
                .sorted()
                .map(Column::getMapper)
                .toList();

        List<String> records = originalData.stream()
                .map(record -> {
                    List<String> fields = new ArrayList<>();

                    for (Function<T, Object> mapper : mappers) {
                        fields.add(mapper.apply(record).toString());
                    }

                    return String.join(fieldSeparator, fields);
                }).toList();

        File file = getFile();
        try (FileWriter writer = new FileWriter(file)) {
            write(writer, String.join(fieldSeparator, headers));
            write(writer, System.lineSeparator());
            records.forEach(s -> {
                write(writer, s);
                write(writer, System.lineSeparator());
            });
        } catch (IOException e) {
            throw new ExportException(e.getMessage(), e);
        }

        return file;
    }
}
