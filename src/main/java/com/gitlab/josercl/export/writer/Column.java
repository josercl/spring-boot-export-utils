package com.gitlab.josercl.export.writer;

import java.util.function.Function;

public final class Column<T> implements Comparable<Column<T>> {
    private final Integer order;
    private final String header;
    private final Function<T, Object> mapper;

    private Column(Integer order, String header, Function<T, Object> mapper) {
        this.order = order;
        this.header = header;
        this.mapper = mapper;
    }

    public Integer getOrder() {
        return order;
    }

    public String getHeader() {
        return header;
    }

    public Function<T, Object> getMapper() {
        return mapper;
    }

    public static <U> Builder<U> builder() {
        return new Builder<>();
    }

    @SuppressWarnings("unchecked")
    public static class Builder<U> {
        private Integer order = 0;
        private String header;
        private Function<U, Object> mapper = (Function<U, Object>) Function.identity();

        public Builder<U> order(Integer o) {
            this.order = o;
            return this;
        }

        public Builder<U> header(String h) {
            this.header = h;
            return this;
        }

        public Builder<U> mapper(Function<U, Object> m) {
            this.mapper = m;
            return this;
        }

        public Column<U> build() {
            return new Column<>(order, header, mapper);
        }
    }


    @Override
    public int compareTo(Column<T> tColumn) {
        if (order.equals(tColumn.getOrder())) {
            return header.compareTo(tColumn.getHeader());
        }

        return order.compareTo(tColumn.getOrder());
    }

}
