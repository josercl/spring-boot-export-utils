package com.gitlab.josercl.export.writer;

import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.exception.ExportException;
import com.gitlab.josercl.export.util.StringUtils;
import org.apache.logging.log4j.util.TriConsumer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class ExcelWriter extends Writer {

    public interface ContentCellCustomizer extends TriConsumer<XSSFCellStyle, XSSFWorkbook, Integer> {}
    public interface HeaderCellCustomizer extends BiConsumer<XSSFCellStyle, XSSFWorkbook> {}

    private final String dateFormat;
    private final int decimalPlaces;
    private final HeaderCellCustomizer headerStyleCustomizer;
    private final ContentCellCustomizer contentStyleCustomizer;

    public ExcelWriter(String dateFormat, int decimalPlaces) {
        this.dateFormat = dateFormat;
        this.decimalPlaces = decimalPlaces;
        this.contentStyleCustomizer = (ignored, ignored2, ignoredRow) -> {};
        this.headerStyleCustomizer = (ignored, ignored2) -> {};
    }

    public ExcelWriter(String dateFormat, int decimalPlaces, HeaderCellCustomizer headerStyleCustomizer, ContentCellCustomizer contentStyleCustomizer) {
        this.dateFormat = dateFormat;
        this.decimalPlaces = decimalPlaces;
        this.contentStyleCustomizer = contentStyleCustomizer;
        this.headerStyleCustomizer = headerStyleCustomizer;
    }

    @Override
    public boolean canWrite(FileType fileType) {
        return FileType.EXCEL.equals(fileType);
    }

    @Override
    public <T> File toFile(Collection<T> originalData, Set<Column<T>> columns) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        DataFormat dataFormat = workbook.createDataFormat();
        CreationHelper creationHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Data");

        XSSFFont headerFont = workbook.createFont();
        headerFont.setBold(true);

        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(headerFont);
        headerStyleCustomizer.accept(headerStyle, workbook);

        XSSFCellStyle baseStyle = workbook.createCellStyle();

        XSSFCellStyle stringStyle = workbook.createCellStyle();
        stringStyle.cloneStyleFrom(baseStyle);

        XSSFCellStyle intStyle = workbook.createCellStyle();
        intStyle.cloneStyleFrom(baseStyle);

        XSSFCellStyle booleanStyle = workbook.createCellStyle();
        booleanStyle.cloneStyleFrom(baseStyle);

        XSSFCellStyle dateStyle = workbook.createCellStyle();
        dateStyle.cloneStyleFrom(baseStyle);

        if (Objects.nonNull(dateFormat)) {
            dateStyle.setDataFormat(creationHelper.createDataFormat().getFormat(dateFormat));
        }

        XSSFCellStyle floatStyle = workbook.createCellStyle();
        floatStyle.cloneStyleFrom(baseStyle);
        floatStyle.setDataFormat(dataFormat.getFormat(StringUtils.rightPad("00.", decimalPlaces + 3).replaceAll("\\s", "0")));

        Map<Class<?>, CellStyle> styles = new HashMap<>();
        styles.put(String.class, stringStyle);
        styles.put(Boolean.class, booleanStyle);
        styles.put(Integer.class, intStyle);
        styles.put(Long.class, intStyle);
        styles.put(Float.class, floatStyle);
        styles.put(Double.class, floatStyle);
        styles.put(LocalDate.class, dateStyle);
        styles.put(LocalDateTime.class, dateStyle);
        styles.put(Date.class, dateStyle);

        List<String> headers = columns.stream()
                .sorted()
                .map(Column::getHeader)
                .toList();

        List<Function<T, Object>> mappers = columns.stream()
                .sorted()
                .map(Column::getMapper)
                .toList();

        writeHeaders(sheet, headerStyle, headers);

        originalData.forEach(record -> {
            Row row = sheet.createRow(sheet.getLastRowNum() + 1);

            for (Function<T, Object> mapper : mappers) {
                Object value = mapper.apply(record);

                Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));

                XSSFCellStyle cellStyle = workbook.createCellStyle();
                cellStyle.cloneStyleFrom(styles.getOrDefault(value.getClass(), stringStyle));
                contentStyleCustomizer.accept(cellStyle, workbook, row.getRowNum());

                cell.setCellStyle(cellStyle);
                write(cell, value);
            }
        });

        File file = getFile();
        try(FileOutputStream outputStream = new FileOutputStream(file)) {
            workbook.write(outputStream);
        } catch (IOException e) {
            throw new ExportException(e.getMessage(), e);
        }
        return file;
    }

    private void writeHeaders(Sheet sheet, CellStyle cellStyle, List<String> headers) {
        Row row = sheet.createRow(0);
        headers.forEach(header -> {
            Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));
            cell.setCellStyle(cellStyle);
            write(cell, header);
        });
    }

    private void write(Cell cell, Object value) {
        if (value instanceof Integer || value instanceof Long) {
            cell.setCellValue(Double.parseDouble(value.toString()));
            return;
        }
        if (value instanceof LocalDateTime) {
            cell.setCellValue((LocalDateTime) value);
            return;
        }
        if (value instanceof LocalDate) {
            cell.setCellValue((LocalDate) value);
            return;
        }
        cell.setCellValue(value.toString());
    }
}