package com.gitlab.josercl.export.writer;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.exception.ExportException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class XMLWriter extends Writer {
    private final XmlMapper xmlMapper;

    public XMLWriter(XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    @Override
    public boolean canWrite(FileType fileType) {
      return FileType.XML.equals(fileType);
    }

    @Override
    public <T> File toFile(Collection<T> originalData, Set<Column<T>> columns) {
        List<Column<T>> columnList = columns.stream().sorted().toList();

        List<Map<String, Object>> records = originalData.stream()
                .map(record -> {
                    Map<String, Object> obj = new HashMap<>();

                    for (Column<T> tColumn : columnList) {
                        obj.put(tColumn.getHeader(), tColumn.getMapper().apply(record));
                    }

                    return obj;
                }).toList();

        File file = getFile();
        try(FileWriter writer = new FileWriter(file)) {
            write(writer, xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(records));
        } catch (IOException e) {
            throw new ExportException(e.getMessage(), e);
        }
        return file;
    }
}
