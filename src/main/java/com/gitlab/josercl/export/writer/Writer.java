package com.gitlab.josercl.export.writer;

import com.gitlab.josercl.export.enums.FileType;
import com.gitlab.josercl.export.exception.ExportException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Collection;
import java.util.Set;

public abstract class Writer {

    public abstract boolean canWrite(FileType fileType);

    public abstract <T> File toFile(Collection<T> originalData, Set<Column<T>> columns);

    protected Path getDirectory() {
        Path directory = Paths.get(System.getProperty("java.io.tmpdir"), "reports");

        if (directory.toFile().exists()) {
            return directory;
        }

        try {
            return Files.createDirectory(directory);
        } catch (IOException e) {
            throw new ExportException(e.getMessage(), e);
        }
    }

    protected void write(FileWriter writer, String text) {
        try {
            writer.write(text);
        } catch (IOException exception) {
            throw new ExportException(exception.getMessage(), exception);
        }
    }

    protected File getFile() {
        return getDirectory().resolve(String.valueOf(Calendar.getInstance().getTimeInMillis())).toFile();
    }
}
