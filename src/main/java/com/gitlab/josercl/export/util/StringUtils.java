package com.gitlab.josercl.export.util;

public class StringUtils {
    public static String rightPad(String string, int size) {
        return String.format("%-" + size + "s", string);
    }
}
