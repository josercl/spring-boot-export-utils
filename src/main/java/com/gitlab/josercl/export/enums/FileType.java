package com.gitlab.josercl.export.enums;

import java.util.List;

public enum FileType {
    TXT(List.of("TXT", "TEXT"), "text/plain", "txt"),
    EXCEL(List.of("XLSX", "XLS", "EXCEL"), "application/vnd.ms-excel", "xlsx"),
    CSV(List.of("CSV"), "text/csv", "csv"),
    JSON(List.of("JSON"), "application/json", "json"),
    XML(List.of("XML"), "text/xml", "xml")
    ;

    private final List<String> codes;
    private final String mediaType;
    private final String extension;

    FileType(List<String> codes, String mediaType, String extension) {
        this.codes = codes;
        this.mediaType = mediaType;
        this.extension = extension;
    }

    public List<String> getCodes() {
        return codes;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getExtension() {
        return extension;
    }
}
